import React from "react";
import { Redirect, Route } from 'react-router-dom';
import {
  IonApp,
  IonRouterOutlet,
} from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import Login from "./pages/Login/Login";
import Home from "./pages/Home/Home";
import Perfil from "./pages/Perfil/Perfil";
import Compras from "./pages/Compras/Compras";
import Pagamentos from "./pages/Pagamentos/Pagamentos";
import Notificacoes from "./pages/Notificacoes/Notificacoes";
import Credito from "./pages/Credito/Credito";
import Contato from "./pages/Contato/Contato";

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import './pages/style.css'
import InformacoesPessoais from "./pages/Perfil/Pessoal/InformacoesPessoais";
import InformacoesEndereco from "./pages/Perfil/Endereco/InformacoesEndereco";
import InformacoesSenha from "./pages/Perfil/Senha/InformacoesSenha";
import InformacoesTermos from "./pages/Perfil/Termos/InformacoesTermos";


const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <IonRouterOutlet>
        <Route path="/" exact>
          <Login />
        </Route>
        <Route path="/Home" >
          <Home notificationHeader={true} menuBottomRoutes={true} titulo={["Seja", <br />, "Bem-Vindo!"]}/>
        </Route>
        <Route path="/Compras">
          <Compras notificationHeader={true} menuBottomRoutes={true} titulo={["Minhas", <br />, "Compras"]} />
        </Route>
        <Route path="/Pagamentos">
          <Pagamentos notificationHeader={true} menuBottomRoutes={true} titulo={["Meus", <br />, "Pagamentos"]} />
        </Route>
        <Route path="/Duvidas">
          <Redirect to="/Home" />
        </Route>
        <Route path="/Credito">
          <Credito notificationHeader={true} menuBottomRoutes={true} titulo={["Limite", <br />, "de Crédito"]}/>
        </Route>
        <Route path="/Contato">
          <Contato notificationHeader={true} menuBottomRoutes={true} titulo={["Entre em", <br />, "Contato"]} />
        </Route>
        <Route path="/Perfil/" exact>
          <Perfil versionFooter={true} menuBottomRoutes={false} showBackButton={true} />
        </Route>
        <Route path="/Perfil/Pessoal/" exact>
          <InformacoesPessoais/>
        </Route>
        <Route path="/Perfil/Endereco/" exact>
          <InformacoesEndereco/>
        </Route>
        <Route path="/Perfil/Senha/" exact>
          <InformacoesSenha/>
        </Route>
        <Route path="/Perfil/Termos/" exact>
          <InformacoesTermos/>
        </Route>
        <Route path="/Notificacoes">
          <Notificacoes showBackButton={true} />
        </Route>
      </IonRouterOutlet>
    </IonReactRouter>
  </IonApp>
);

export default App;
