import React from 'react';
import './Contato.css';
import NotificationHeader from "../../components/notificationHeader";
import {IonButton, IonContent, IonPage, IonItem, IonLabel, IonInput} from "@ionic/react";
import Rotas from "../../routes/Rotas";

const Contato = (props: { notificationHeader: boolean, menuBottomRoutes: boolean, titulo: any }) => {
  return (
    <IonPage>
      { props.notificationHeader && <NotificationHeader />}
      <IonContent className="ion-padding page-contato scrollable">
        <h2 key="keyContato" className="tituloPagina ion-text-center">{props.titulo}</h2>
          <form className="ion-padding">
            <IonItem>
              <IonLabel position="floating">Nome</IonLabel>
              <IonInput />
            </IonItem>
            <IonItem>
              <IonLabel position="floating">E-mail</IonLabel>
              <IonInput type="email" />
            </IonItem>
            <IonItem>
              <IonLabel position="floating">Telefone</IonLabel>
              <IonInput type="number" />
            </IonItem>
            <IonItem>
              <IonLabel position="floating">Assunto</IonLabel>
              <IonInput />
            </IonItem>
            <IonItem>
              <IonLabel position="floating">Mensagem</IonLabel>
              <IonInput />
            </IonItem>
            <IonButton className="ion-margin-top" type="submit" expand="block">
              Enviar
            </IonButton>
          </form>
        </IonContent>
      { props.menuBottomRoutes && <Rotas /> }
    </IonPage>
  )
}

export default Contato;