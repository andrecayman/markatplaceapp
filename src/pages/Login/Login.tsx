import React from 'react';
import '../Login/Login.css';
import { Link } from 'react-router-dom';
import {
  IonContent,
  IonImg,
  IonItem,
  IonLabel,
  IonInput,
  IonButton,
  IonGrid,
  IonRow,
  IonCol,
  IonIcon
} from '@ionic/react';
import {lockClosedOutline} from "ionicons/icons";

const Login = () => {
  const logoCliente = "/assets/logo-markatplace.png";

  function checkUser(event:any) {
    event.preventDefault();
  }

  return (
    <IonContent className="page-login ion-padding">
      <IonGrid>
        <IonRow className="ion-align-items-start">
          <IonCol className="conteudo-principal">
            <IonImg style={{ width: '198px'}} alt="Logo Markatplace" className="logo-cliente" src={logoCliente} />
            <div className="Conteúdo">
              <h1 className="titulo ion-text-center">Área<br />do cliente</h1>
              <form onSubmit={checkUser}>
                <IonItem>
                  <IonLabel position="floating">E-mail</IonLabel>
                  <IonInput
                    type="email"
                  />
                </IonItem>
                <IonItem>
                  <IonLabel position="floating">Senha</IonLabel>
                  <IonInput
                    type="password"
                  />
                </IonItem>
                <IonButton  className="ion-margin-top botao-login" type="submit" expand="block">
                  Entrar
                </IonButton>
              </form>
            </div>
            <Link className="ion-text-center" to="/Home">Entrar sem acesso </Link>
            <div className="digital-protegido">
              <IonIcon slot="icon-only" icon={lockClosedOutline} />
              <IonLabel>Ambiente digital protegido!</IonLabel>
            </div>
          </IonCol>
        </IonRow>
      </IonGrid>
    </IonContent>
  )
}

export default Login;
