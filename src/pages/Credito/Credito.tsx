import React from 'react';
import {
  IonCard,
  IonContent,
  IonPage,
  IonTitle,
  IonButton
} from "@ionic/react";
import NotificationHeader from "../../components/notificationHeader";
import Rotas from "../../routes/Rotas";
import './Credito.css'


const Credito = (props: { notificationHeader: boolean, menuBottomRoutes: boolean, titulo: any }) => {
  return (
    <IonPage>
      { props.notificationHeader && <NotificationHeader />}
      <IonContent className="ion-padding page-credito scrollable">
        <h2 key="keyCredito" className="tituloPagina ion-text-center">{props.titulo}</h2>
        <IonCard>
          <IonTitle>Disponível para compra:</IonTitle>
          <p className="disponivel">R$ 1189,00</p>
        </IonCard>
        <IonCard>
          <IonTitle>Utilizado</IonTitle>
          <p className="usado">R$ 1189,00</p>
        </IonCard>
        <IonButton>
          Solicitar mais limite
        </IonButton>
      </IonContent>
      { props.menuBottomRoutes && <Rotas />}
    </IonPage>
  )
}

export default Credito;