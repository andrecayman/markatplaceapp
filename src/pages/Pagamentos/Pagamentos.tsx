import React from 'react';
import '../Pagamentos/Pagamentos.css';
import {
  IonPage,
  IonContent,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
} from '@ionic/react';
import NotificationHeader from "../../components/notificationHeader";
import Rotas from "../../routes/Rotas";

const Pagamentos = (props: { notificationHeader: boolean, menuBottomRoutes: boolean, titulo: any }) => {
  return (
    <IonPage>
      { props.notificationHeader && <NotificationHeader />}
      <IonContent className="ion-padding page-pagamentos scrollable">
        <h2 key="keyPagamentos" className="tituloPagina ion-text-center">{props.titulo}</h2>
        <IonCard className="card-pagamentos ion-justify-content-between ion-align-items-start flex">
          <IonCardHeader className="ion-float-left">
            <IonCardSubtitle>11/11/2020</IonCardSubtitle>
            <IonCardSubtitle>Compra Código #16 </IonCardSubtitle>
            <IonCardTitle className="status efetuado">Pagamento Efetuado</IonCardTitle>
          </IonCardHeader>
          <IonCardContent className="ion-float-right">
            <IonCard className="preco">
              R$ 40,00
            </IonCard>
          </IonCardContent>
        </IonCard>
        <IonCard className="card-pagamentos ion-justify-content-between ion-align-items-start flex">
          <IonCardHeader className="ion-float-left">
            <IonCardSubtitle>11/11/2020</IonCardSubtitle>
            <IonCardSubtitle>Compra Código #16 </IonCardSubtitle>
            <IonCardTitle className="status cancelado">Pagamento cancelado</IonCardTitle>
          </IonCardHeader>
          <IonCardContent className="ion-float-right">
            <IonCard className="preco">
              R$ 40,00
            </IonCard>
          </IonCardContent>
        </IonCard>
        <IonCard className="card-pagamentos ion-justify-content-between ion-align-items-start flex">
          <IonCardHeader className="ion-float-left">
            <IonCardSubtitle>11/11/2020</IonCardSubtitle>
            <IonCardSubtitle>Compra Código #16 </IonCardSubtitle>
            <IonCardTitle className="status efetuado">Pagamento Efetuado</IonCardTitle>
          </IonCardHeader>
          <IonCardContent className="ion-float-right">
            <IonCard className="preco">
              R$ 40,00
            </IonCard>
          </IonCardContent>
        </IonCard>
        <IonCard className="card-pagamentos ion-justify-content-between ion-align-items-start flex">
          <IonCardHeader className="ion-float-left">
            <IonCardSubtitle>11/11/2020</IonCardSubtitle>
            <IonCardSubtitle>Compra Código #16 </IonCardSubtitle>
            <IonCardTitle className="status efetuado">Pagamento Efetuado</IonCardTitle>
          </IonCardHeader>
          <IonCardContent className="ion-float-right">
            <IonCard className="preco">
              R$ 40,00
            </IonCard>
          </IonCardContent>
        </IonCard>
        <IonCard className="card-pagamentos ion-justify-content-between ion-align-items-start flex">
          <IonCardHeader className="ion-float-left">
            <IonCardSubtitle>11/11/2020</IonCardSubtitle>
            <IonCardSubtitle>Compra Código #16 </IonCardSubtitle>
            <IonCardTitle className="status efetuado">Pagamento Efetuado</IonCardTitle>
          </IonCardHeader>
          <IonCardContent className="ion-float-right">
            <IonCard className="preco">
              R$ 40,00
            </IonCard>
          </IonCardContent>
        </IonCard>
        <IonCard className="card-pagamentos ion-justify-content-between ion-align-items-start flex">
          <IonCardHeader className="ion-float-left">
            <IonCardSubtitle>11/11/2020</IonCardSubtitle>
            <IonCardSubtitle>Compra Código #16 </IonCardSubtitle>
            <IonCardTitle className="status efetuado">Pagamento Efetuado</IonCardTitle>
          </IonCardHeader>
          <IonCardContent className="ion-float-right">
            <IonCard className="preco">
              R$ 40,00
            </IonCard>
          </IonCardContent>
        </IonCard>
        <IonCard className="card-pagamentos ion-justify-content-between ion-align-items-start flex">
          <IonCardHeader className="ion-float-left">
            <IonCardSubtitle>11/11/2020</IonCardSubtitle>
            <IonCardSubtitle>Compra Código #16 </IonCardSubtitle>
            <IonCardTitle className="status efetuado">Pagamento Efetuado</IonCardTitle>
          </IonCardHeader>
          <IonCardContent className="ion-float-right">
            <IonCard className="preco">
              R$ 40,00
            </IonCard>
          </IonCardContent>
        </IonCard>
      </IonContent>
      { props.menuBottomRoutes && <Rotas />}
    </IonPage>
  )
}

export default Pagamentos;