import React from 'react';
import {
  IonBadge,
  IonButtons,
  IonContent,
  IonHeader,
  IonItem,
  IonLabel,
  IonPage,
  IonToolbar,
  IonList
} from "@ionic/react";
import BackButton from "../../components/BackButton";
import './Notificacoes.css';

const Notificacoes = (props: { showBackButton: boolean }) => {
  return (
    <IonPage>
      <IonHeader className="header-notificacoes ">
        <IonToolbar className="toolbar">
          <IonButtons slot="start">
            { props.showBackButton && <BackButton /> }
            <p className="subtitulo">Notificações</p>
          </IonButtons>
        </IonToolbar>
      </IonHeader>

      <IonContent className="page-notificacoes">
        <IonList>
          <IonItem>
            <IonLabel>Pokémon Yellow</IonLabel>
          </IonItem>
          <IonItem>
            <IonLabel>Mega Man X</IonLabel>
          </IonItem>
          <IonItem>
            <IonLabel>The Legend of Zelda</IonLabel>
          </IonItem>
          <IonItem>
            <IonLabel>Pac-Man</IonLabel>
          </IonItem>
          <IonItem>
            <IonLabel>Super Mario World</IonLabel>
          </IonItem>
        </IonList>
      </IonContent>

    </IonPage>
  )
}

export default Notificacoes;