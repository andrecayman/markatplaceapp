import React from 'react';
import '../Compras/Compras.css';
import {
  IonPage,
  IonContent,
  IonCard,
  IonCardContent,
  IonCardHeader,
  IonCardSubtitle,
  IonCardTitle,
  IonIcon,
  IonHeader,
  IonLabel
} from '@ionic/react';
import NotificationHeader from "../../components/notificationHeader";
import Rotas from "../../routes/Rotas";
import {cartOutline} from "ionicons/icons";

const Compras = (props: { notificationHeader: boolean, menuBottomRoutes: boolean, titulo: any }) => {
  return (
    <IonPage>
      { props.notificationHeader && <NotificationHeader />}
      <IonContent className="ion-padding page-compras scrollable">
        <h2 key="keyCompras" className="tituloPagina ion-text-center">{props.titulo}</h2>
        <IonCard className="card-compras ion-justify-content-between ion-align-items-center flex">
          <IonCardHeader className="ion-float-left">
            <IonCardSubtitle>Compra Código #16 </IonCardSubtitle>
            <IonCardSubtitle>R$ 40,00 </IonCardSubtitle>
            <IonCardTitle className="status efetuado">Pagamento Efetuado</IonCardTitle>
          </IonCardHeader>
          <IonCardContent className="ion-float-right">
            <IonCard href="/home" className="s-via-boleto ion-justify-content-center">
              <IonHeader className="ion-text-center">
                <IonIcon size="large" icon={cartOutline} />
              </IonHeader>
              <IonLabel className="ion-text-center">2º via do <br />boleto</IonLabel>
            </IonCard>
          </IonCardContent>
        </IonCard>
        <IonCard className="card-compras ion-justify-content-between ion-align-items-center flex">
          <IonCardHeader className="ion-float-left">
            <IonCardSubtitle>Compra Código #16 </IonCardSubtitle>
            <IonCardSubtitle>R$ 40,00 </IonCardSubtitle>
            <IonCardTitle className="status n-efetuado">Aguardando Pagamento</IonCardTitle>
          </IonCardHeader>
          <IonCardContent className="ion-float-right">
            <IonCard href="/home" className="s-via-boleto ion-justify-content-center">
              <IonHeader className="ion-text-center">
                <IonIcon size="large" icon={cartOutline} />
              </IonHeader>
              <IonLabel className="ion-text-center">2º via do <br />boleto</IonLabel>
            </IonCard>
          </IonCardContent>
        </IonCard>
        <IonCard className="card-compras ion-justify-content-between ion-align-items-center flex">
          <IonCardHeader className="ion-float-left">
            <IonCardSubtitle>Compra Código #16 </IonCardSubtitle>
            <IonCardSubtitle>R$ 40,00 </IonCardSubtitle>
            <IonCardTitle className="status efetuado">Pagamento Efetuado</IonCardTitle>
          </IonCardHeader>
          <IonCardContent className="ion-float-right">
            <IonCard href="/home" className="s-via-boleto ion-justify-content-center">
              <IonHeader className="ion-text-center">
                <IonIcon size="large" icon={cartOutline} />
              </IonHeader>
              <IonLabel className="ion-text-center">2º via do <br />boleto</IonLabel>
            </IonCard>
          </IonCardContent>
        </IonCard>
        <IonCard className="card-compras ion-justify-content-between ion-align-items-center flex">
          <IonCardHeader className="ion-float-left">
            <IonCardSubtitle>Compra Código #16 </IonCardSubtitle>
            <IonCardSubtitle>R$ 40,00 </IonCardSubtitle>
            <IonCardTitle className="status efetuado">Pagamento Efetuado</IonCardTitle>
          </IonCardHeader>
          <IonCardContent className="ion-float-right">
            <IonCard href="/home" className="s-via-boleto ion-justify-content-center">
              <IonHeader className="ion-text-center">
                <IonIcon size="large" icon={cartOutline} />
              </IonHeader>
              <IonLabel className="ion-text-center">2º via do <br />boleto</IonLabel>
            </IonCard>
          </IonCardContent>
        </IonCard>
        <IonCard className="card-compras ion-justify-content-between ion-align-items-center flex">
          <IonCardHeader className="ion-float-left">
            <IonCardSubtitle>Compra Código #16 </IonCardSubtitle>
            <IonCardSubtitle>R$ 40,00 </IonCardSubtitle>
            <IonCardTitle className="status efetuado">Pagamento Efetuado</IonCardTitle>
          </IonCardHeader>
          <IonCardContent className="ion-float-right">
            <IonCard href="/home" className="s-via-boleto ion-justify-content-center">
              <IonHeader className="ion-text-center">
                <IonIcon size="large" icon={cartOutline} />
              </IonHeader>
              <IonLabel className="ion-text-center">2º via do <br />boleto</IonLabel>
            </IonCard>
          </IonCardContent>
        </IonCard>
        <IonCard className="card-compras ion-justify-content-between ion-align-items-center flex">
          <IonCardHeader className="ion-float-left">
            <IonCardSubtitle>Compra Código #16 </IonCardSubtitle>
            <IonCardSubtitle>R$ 40,00 </IonCardSubtitle>
            <IonCardTitle className="status efetuado">Pagamento Efetuado</IonCardTitle>
          </IonCardHeader>
          <IonCardContent className="ion-float-right">
            <IonCard href="/home" className="s-via-boleto ion-justify-content-center">
              <IonHeader className="ion-text-center">
                <IonIcon size="large" icon={cartOutline} />
              </IonHeader>
              <IonLabel className="ion-text-center">2º via do <br />boleto</IonLabel>
            </IonCard>
          </IonCardContent>
        </IonCard>
        <IonCard className="card-compras ion-justify-content-between ion-align-items-center flex">
          <IonCardHeader className="ion-float-left">
            <IonCardSubtitle>Compra Código #16 </IonCardSubtitle>
            <IonCardSubtitle>R$ 40,00 </IonCardSubtitle>
            <IonCardTitle className="status efetuado">Pagamento Efetuado</IonCardTitle>
          </IonCardHeader>
          <IonCardContent className="ion-float-right">
            <IonCard href="/home" className="s-via-boleto ion-justify-content-center">
              <IonHeader className="ion-text-center">
                <IonIcon size="large" icon={cartOutline} />
              </IonHeader>
              <IonLabel className="ion-text-center">2º via do <br />boleto</IonLabel>
            </IonCard>
          </IonCardContent>
        </IonCard>
        <IonCard className="card-compras ion-justify-content-between ion-align-items-center flex">
          <IonCardHeader className="ion-float-left">
            <IonCardSubtitle>Compra Código #16 </IonCardSubtitle>
            <IonCardSubtitle>R$ 40,00 </IonCardSubtitle>
            <IonCardTitle className="status efetuado">Pagamento Efetuado</IonCardTitle>
          </IonCardHeader>
          <IonCardContent className="ion-float-right">
            <IonCard href="/home" className="s-via-boleto ion-justify-content-center">
              <IonHeader className="ion-text-center">
                <IonIcon size="large" icon={cartOutline} />
              </IonHeader>
              <IonLabel className="ion-text-center">2º via do <br />boleto</IonLabel>
            </IonCard>
          </IonCardContent>
        </IonCard>
        <IonCard className="card-compras ion-justify-content-between ion-align-items-center flex">
          <IonCardHeader className="ion-float-left">
            <IonCardSubtitle>Compra Código #16 </IonCardSubtitle>
            <IonCardSubtitle>R$ 40,00 </IonCardSubtitle>
            <IonCardTitle className="status efetuado">Pagamento Efetuado</IonCardTitle>
          </IonCardHeader>
          <IonCardContent className="ion-float-right">
            <IonCard href="/home" className="s-via-boleto ion-justify-content-center">
              <IonHeader className="ion-text-center">
                <IonIcon size="large" icon={cartOutline} />
              </IonHeader>
              <IonLabel className="ion-text-center">2º via do <br />boleto</IonLabel>
            </IonCard>
          </IonCardContent>
        </IonCard>
        <IonCard className="card-compras ion-justify-content-between ion-align-items-center flex">
          <IonCardHeader className="ion-float-left">
            <IonCardSubtitle>Compra Código #16 </IonCardSubtitle>
            <IonCardSubtitle>R$ 40,00 </IonCardSubtitle>
            <IonCardTitle className="status efetuado">Pagamento Efetuado</IonCardTitle>
          </IonCardHeader>
          <IonCardContent className="ion-float-right">
            <IonCard href="/home" className="s-via-boleto ion-justify-content-center">
              <IonHeader className="ion-text-center">
                <IonIcon size="large" icon={cartOutline} />
              </IonHeader>
              <IonLabel className="ion-text-center">2º via do <br />boleto</IonLabel>
            </IonCard>
          </IonCardContent>
        </IonCard>
        <IonCard className="card-compras ion-justify-content-between ion-align-items-center flex">
          <IonCardHeader className="ion-float-left">
            <IonCardSubtitle>Compra Código #16 </IonCardSubtitle>
            <IonCardSubtitle>R$ 40,00 </IonCardSubtitle>
            <IonCardTitle className="status efetuado">Pagamento Efetuado</IonCardTitle>
          </IonCardHeader>
          <IonCardContent className="ion-float-right">
            <IonCard href="/home" className="s-via-boleto ion-justify-content-center">
              <IonHeader className="ion-text-center">
                <IonIcon size="large" icon={cartOutline} />
              </IonHeader>
              <IonLabel className="ion-text-center">2º via do <br />boleto</IonLabel>
            </IonCard>
          </IonCardContent>
        </IonCard>
      </IonContent>
      { props.menuBottomRoutes && <Rotas />}
    </IonPage>
  )
}

export default Compras;