import React from 'react';
import {
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonIcon,
  IonInput,
  IonItem,
  IonLabel,
  IonPage,
  IonToolbar
} from "@ionic/react";
import BackButton from "../../../components/BackButton";
import { lockClosedOutline} from "ionicons/icons";

const InformacoesSenha = () => {
  return (
    <IonPage>
      <IonHeader className="header-perfil ">
        <IonToolbar className="toolbar">
          <IonButtons slot="start">
            <BackButton />
            <IonIcon className="icon-interno" slot="end" icon={lockClosedOutline} />
            <IonLabel>Alterar senha</IonLabel>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent className="page-perfil ion-padding-top">
        <form className="ion-padding">
          <IonItem>
            <IonLabel position="floating">Senha antiga</IonLabel>
            <IonInput />
          </IonItem>
          <IonItem>
            <IonLabel position="floating">Nova senha</IonLabel>
            <IonInput type="password" />
          </IonItem>
          <IonItem>
            <IonLabel position="floating">Repetir senha</IonLabel>
            <IonInput type="password" />
          </IonItem>
          <IonButton className="ion-margin-top" type="submit" expand="block">
            Enviar
          </IonButton>
        </form>
      </IonContent>
    </IonPage>
  )
}

export default InformacoesSenha;