import React from 'react';
import {
  IonContent,
  IonHeader,
  IonToolbar,
  IonButtons,
  IonItem,
  IonLabel,
  IonInput,
  IonPage, IonIcon,
  IonButton
} from '@ionic/react';
import BackButton from "../../../components/BackButton";
import {
  personOutline
} from "ionicons/icons";

const InformacoesPessoais = () => {
  return (
    <IonPage>
        <IonHeader className="header-perfil ">
          <IonToolbar className="toolbar">
            <IonButtons slot="start">
              <BackButton />
              <IonIcon className="icon-interno" slot="end" icon={personOutline} />
              <IonLabel>Informações pessoais</IonLabel>
            </IonButtons>
          </IonToolbar>
        </IonHeader>
        <IonContent className="page-perfil ion-padding-top">
          <form className="ion-padding">
            <IonItem>
              <IonLabel position="floating">Nome</IonLabel>
              <IonInput />
            </IonItem>
            <IonItem>
              <IonLabel position="floating">E-mail</IonLabel>
              <IonInput type="email" />
            </IonItem>
            <IonItem>
              <IonLabel position="floating">Telefone</IonLabel>
              <IonInput type="number" />
            </IonItem>
            <IonItem>
              <IonLabel position="floating">CPF</IonLabel>
              <IonInput />
            </IonItem>
            <IonItem>
              <IonLabel position="floating">Data de nascimento</IonLabel>
              <IonInput type="date" />
            </IonItem>
            <IonItem>
              <IonLabel position="floating">Profissão</IonLabel>
              <IonInput type="text" />
            </IonItem>
            <IonItem>
              <IonLabel position="floating">Nível educacional</IonLabel>
              <IonInput type="text" />
            </IonItem>
            <IonItem>
              <IonLabel position="floating">Sexo</IonLabel>
              <IonInput type="text" />
            </IonItem>
            <IonButton className="ion-margin-top" type="submit" expand="block">
              Enviar
            </IonButton>
          </form>
        </IonContent>
    </IonPage>
  )
}

export default InformacoesPessoais;