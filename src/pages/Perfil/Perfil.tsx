import React from 'react';
import {
  IonContent,
  IonHeader,
  IonToolbar,
  IonButtons,
  IonItem,
  IonLabel,
  IonRouterOutlet,
  IonList,
  IonPage, IonIcon, IonBadge
} from '@ionic/react';

import BackButton from "../../components/BackButton";
import Rotas from "../../routes/Rotas";
import './Perfil.css'
import {
  notificationsOutline,
  personCircleOutline,
  personOutline,
  createOutline,
  lockClosedOutline,
  readerOutline,
  powerOutline, chevronForward
} from "ionicons/icons";
import {Link, Route} from "react-router-dom";
import VersionFooter from "../../components/VersionFooter";
import Notificacoes from "../Notificacoes/Notificacoes";
import {IonReactRouter} from "@ionic/react-router";

const Perfil = (props: { menuBottomRoutes: boolean, showBackButton: boolean, versionFooter: boolean }) => {
  function openPage() {
  }
  return (
      <IonPage>
          <IonHeader className="header-perfil ">
            <IonToolbar className="toolbar">
              <IonButtons slot="start">
                { props.showBackButton && <BackButton /> }
              </IonButtons>
              <IonButtons slot="primary">
                <Link className="container-icon" to="/Notificacoes">
                  <IonIcon  slot="end" icon={notificationsOutline} />
                  <IonBadge color="danger">.</IonBadge>
                </Link>
              </IonButtons>
            </IonToolbar>
            <IonItem className="usuario">
              <IonIcon icon={personCircleOutline} />
              <IonLabel> André Bez Birolo </IonLabel>
            </IonItem>
          </IonHeader>
          <IonContent className="page-perfil ion-padding-top">
            <IonList>
              <IonButtons slot="primary">
                <Link className="container-icon" to="/Perfil/Pessoal/">
                  <div>
                    <IonIcon  slot="end" icon={personOutline} />
                    <IonLabel>Informações pessoais</IonLabel>
                  </div>
                  <IonIcon slot="end" icon={chevronForward} />
                </Link>
              </IonButtons>
              <IonButtons slot="primary">
                <Link className="container-icon" to="/Perfil/Endereco/">
                  <div>
                    <IonIcon  slot="end" icon={createOutline} />
                    <IonLabel>Informações de endereço</IonLabel>
                  </div>
                  <IonIcon slot="end" icon={chevronForward} />
                </Link>
              </IonButtons>
              <IonButtons slot="primary">
                <Link className="container-icon" to="/Perfil/Senha/">
                  <div>
                    <IonIcon  slot="end" icon={lockClosedOutline} />
                    <IonLabel>Alterar senha</IonLabel>
                  </div>
                  <IonIcon slot="end" icon={chevronForward} />
                </Link>
              </IonButtons>
              <IonButtons slot="primary">
                <Link className="container-icon" to="/Perfil/Termos/">
                  <div>
                    <IonIcon  slot="end" icon={readerOutline} />
                    <IonLabel>Termos de privacidade</IonLabel>
                  </div>
                  <IonIcon slot="end" icon={chevronForward} />
                </Link>
              </IonButtons>
              <IonButtons slot="primary">
                <Link className="container-icon" to="/">
                  <div>
                    <IonIcon  slot="end" icon={powerOutline} />
                    <IonLabel>Sair</IonLabel>
                  </div>
                </Link>
              </IonButtons>
            </IonList>
          </IonContent>
          { props.menuBottomRoutes && <Rotas />}
          { props.versionFooter && <VersionFooter />}
      </IonPage>
  )
}

export default Perfil;