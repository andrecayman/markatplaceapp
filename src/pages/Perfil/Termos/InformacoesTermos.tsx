import React, {useEffect, useState} from 'react';
import {
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonIcon,
  IonInput,
  IonItem,
  IonLabel,
  IonPage,
  IonToolbar
} from "@ionic/react";
import BackButton from "../../../components/BackButton";
import {createOutline, readerOutline} from "ionicons/icons";

const InformacoesTermos = () => {
  const termosURL:string = 'https://api.chucknorris.io/jokes/random';
  const [termos, setTermos] = useState(null);

  useEffect(() => {
    // useEffect(() => {
    //   async function fetchTermos() {
    //     const response =  await fetch(termosURL)
    //     return await response.json();
    //   }
    //   fetchTermos().then(r => {
    //     setTermos(r.value);
    //   })
    //
    // }, [termosURL] )
    // ou
    fetch(termosURL)
      .then(r => {
        return r.json()
      })
      .then(terms => {
        setTermos(terms.value);
      })
  }, [termosURL] )

  return (
    <IonPage>
      <IonHeader className="header-perfil ">
        <IonToolbar className="toolbar">
          <IonButtons slot="start">
            <BackButton />
            <IonIcon className="icon-interno" slot="end" icon={readerOutline} />
            <IonLabel>Termos de privacidade</IonLabel>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent className="page-perfil ion-padding">
        <h2>Termos de privacidade</h2>
        <p className="termos-conteudo">{termos}</p>
      </IonContent>
    </IonPage>
  )
}

export default InformacoesTermos;