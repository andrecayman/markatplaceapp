import React from 'react';
import {
  IonButton,
  IonButtons,
  IonContent,
  IonHeader,
  IonIcon,
  IonInput,
  IonItem,
  IonLabel,
  IonPage,
  IonToolbar
} from "@ionic/react";
import BackButton from "../../../components/BackButton";
import {createOutline} from "ionicons/icons";

const InformacoesEndereco = () => {
  return (
    <IonPage>
      <IonHeader className="header-perfil ">
        <IonToolbar className="toolbar">
          <IonButtons slot="start">
            <BackButton />
            <IonIcon className="icon-interno" slot="end" icon={createOutline} />
            <IonLabel>Informações de endereço</IonLabel>
          </IonButtons>
        </IonToolbar>
      </IonHeader>
      <IonContent className="page-perfil ion-padding-top">
        <form className="ion-padding">
          <IonItem>
            <IonLabel position="floating">CEP</IonLabel>
            <IonInput />
          </IonItem>
          <IonItem>
            <IonLabel position="floating">UF</IonLabel>
            <IonInput />
          </IonItem>
          <IonItem>
            <IonLabel position="floating">Cidade</IonLabel>
            <IonInput />
          </IonItem>
          <IonItem>
            <IonLabel position="floating">Bairro</IonLabel>
            <IonInput />
          </IonItem>
          <IonItem>
            <IonLabel position="floating">Rua</IonLabel>
            <IonInput />
          </IonItem>
          <IonItem>
            <IonLabel position="floating">Numero</IonLabel>
            <IonInput />
          </IonItem>
          <IonItem>
            <IonLabel position="floating">Complemento</IonLabel>
            <IonInput/>
          </IonItem>
          <IonItem>
            <IonLabel position="floating">Sexo</IonLabel>
            <IonInput type="text" />
          </IonItem>
          <IonButton className="ion-margin-top" type="submit" expand="block">
            Enviar
          </IonButton>
        </form>
      </IonContent>
    </IonPage>
  )
}

export default InformacoesEndereco;