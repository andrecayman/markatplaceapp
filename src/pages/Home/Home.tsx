import React from 'react';
import {
  IonContent,
  IonCard,
  IonCardHeader,
  IonIcon,
  IonPage
} from '@ionic/react';
import {
  analyticsOutline,
  cartOutline,
  cashOutline,
  chatbubbleEllipsesOutline,
  mailOutline,
  personOutline
} from "ionicons/icons";
import {Link} from 'react-router-dom';
import '../Home/Home.css';
import Rotas from "../../routes/Rotas";
import NotificationHeader from "../../components/notificationHeader";

const Home = (props: { notificationHeader: boolean, menuBottomRoutes: boolean, titulo: any }) => {
  return (
    <>
      <IonPage>
        { props.notificationHeader && <NotificationHeader />}
        <IonContent fullscreen key={props.titulo} className="page-home ion-padding">
          <h2 key="keyHome" className="tituloPagina ion-text-center">{props.titulo}</h2>
          <div className="actions-list">
            <Link to="/Compras">
              <IonCard>
                <IonCardHeader className="ion-text-center">
                  <IonIcon icon={cartOutline} />
                  <h2 className="titulo-card">Minhas Compras</h2>
                </IonCardHeader>
              </IonCard>
            </Link>
            <Link to="/Pagamentos">
              <IonCard>
                <IonCardHeader className="ion-text-center">
                  <IonIcon  icon={cashOutline} />
                  <h2 className="titulo-card">Pagamentos</h2>
                </IonCardHeader>
              </IonCard>
            </Link>
            <Link to="/Duvidas">
              <IonCard>
                <IonCardHeader className="ion-text-center">
                  <IonIcon  icon={chatbubbleEllipsesOutline} />
                  <h2 className="titulo-card">Dúvidas frequentes</h2>
                </IonCardHeader>
              </IonCard>
            </Link>
            <Link to="/Credito">
              <IonCard>
                <IonCardHeader className="ion-text-center">
                  <IonIcon  icon={analyticsOutline} />
                  <h2 className="titulo-card">Limite de crédito</h2>
                </IonCardHeader>
              </IonCard>
            </Link>
            <Link to="/Contato">
              <IonCard>
                <IonCardHeader className="ion-text-center">
                  <IonIcon  icon={mailOutline} />
                  <h2 className="titulo-card">Entre em contato</h2>
                </IonCardHeader>
              </IonCard>
            </Link>
            <Link to="/Perfil/">
              <IonCard>
                <IonCardHeader className="ion-text-center">
                  <IonIcon  icon={personOutline} />
                  <h2 className="titulo-card">Perfil</h2>
                </IonCardHeader>
              </IonCard>
            </Link>
          </div>
        </IonContent>
        { props.menuBottomRoutes && <Rotas />}
      </IonPage>
    </>
  )
}

export default Home;