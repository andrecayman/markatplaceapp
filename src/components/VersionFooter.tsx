import React from 'react';

const VersionFooter = () => {
  return (
    <div className="version-footer">
      <p className="ion-text-center">Versão 1.1.1.0</p>
    </div>
  )
}

export default VersionFooter;