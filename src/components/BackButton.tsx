import React from 'react';
import {IonBackButton} from "@ionic/react";

const BackButton = () => {
  return (
    <IonBackButton />
  )
}

export default BackButton;