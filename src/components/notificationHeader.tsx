import React from 'react';
import {
  IonToolbar,
  IonButtons,
  IonIcon,
  IonHeader,
  IonBadge
} from '@ionic/react';
import {personCircleOutline} from "ionicons/icons";
import {Link} from 'react-router-dom';

const NotificationHeader = () => {
  return (
    <IonHeader className="notification-header ion-no-border">
      <IonToolbar>
        <IonButtons slot="primary">
          <Link className="container-icon" to="/Perfil/">
            <IonIcon color="dark" slot="icon-only" icon={personCircleOutline} />
            <IonBadge color="danger">.</IonBadge>
          </Link>
        </IonButtons>
      </IonToolbar>
    </IonHeader>
  )
}

export default NotificationHeader;