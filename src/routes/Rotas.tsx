import React from 'react';
import {IonIcon, IonTabBar, IonTabButton, IonTabs, IonLabel} from "@ionic/react";
import {cartOutline, cashOutline, chatbubbleEllipsesOutline, homeOutline} from "ionicons/icons";
import {Link, NavLink} from 'react-router-dom';

const Rotas = () => {
  return (
    <div className="routes-navigation">
      <NavLink to="/Home">
        <IonIcon icon={homeOutline} />
        <IonLabel>Home</IonLabel>
      </NavLink>
      <NavLink to="/Compras">
        <IonIcon icon={cartOutline} />
        <IonLabel text-wrap>Minhas<br />Compras</IonLabel>
      </NavLink>
      <NavLink to="/Pagamentos">
        <IonIcon icon={cashOutline} />
        <IonLabel>Pagamentos</IonLabel>
      </NavLink>
      <NavLink to="/Duvidas">
        <IonIcon icon={chatbubbleEllipsesOutline} />
        <IonLabel text-wrap>Dúvidas<br />Frequentes</IonLabel>
      </NavLink>
    </div>
  )
}

export default Rotas;